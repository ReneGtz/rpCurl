<?php

namespace rp {
	
	use Exception;
	use const CURLOPT_COOKIEFILE;
	use const CURLOPT_COOKIEJAR;
	use const CURLOPT_HEADER;
	use const CURLOPT_RETURNTRANSFER;
	use const CURLOPT_SSL_VERIFYHOST;
	use const CURLOPT_SSL_VERIFYPEER;
	
	class rpCurl {
		//region init
		/**
		 * the handle that is used for the curl functions
		 *
		 * @var resource|false
		 */
		private $init;
		
		/**
		 * rpCurl constructor.
		 *
		 * @param string|null $url <b>string</b> the url to send a request to<br/><b>null</b> if the url should not be set now
		 *
		 * @throws Exception
		 */
		public function __construct( ?string $url = null, bool $useCookie = false ){
			if( $url === null ){
				$this->init = curl_init();
			} else {
				$this->init = curl_init($url);
			}
			
			if( $this->init === false ){
				throw new Exception('cURL init could not be astonished for "' . $url . '"');
			}
			$this->setOption(CURLOPT_HEADER, true);
			$this->setOption(CURLOPT_RETURNTRANSFER, true);
			$this->setOption(CURLINFO_HEADER_OUT, true);
			
			if( $useCookie ){
				$this->setOption(CURLOPT_COOKIEJAR, 'cookies/jar.txt');
				$this->setOption(CURLOPT_COOKIEFILE, 'cookies/file.txt');
			}
		} //end function __construct()
		
		public function __destruct(){
			curl_close($this->init);
		} //end function __destruct()
		//endregion
		
		//region options
		/**
		 * set an option for the current init
		 *
		 * @param $option mixed the option constant which should be set
		 * @param $value mixed the value to which the option be should set
		 *
		 * @return bool <b>true</b> on success.<br/><b>false</b> on failure.
		 */
		public function setOption( mixed $option, mixed $value ) :bool{
			$result = curl_setopt($this->init, $option, $value);
			if( $result === false ){
				$this->setLastError(curl_errno($this->init), curl_error($this->init));
				return false;
			}
			return true;
		} //end function setOption()
		
		public function disableSSLVerification(){
			$this->setOption(CURLOPT_SSL_VERIFYHOST, 0);
			$this->setOption(CURLOPT_SSL_VERIFYPEER, 0);
		} //end function disableSalVerification()
		
		public function enableSSLVerification(){
			$this->setOption(CURLOPT_SSL_VERIFYHOST, 2);
			$this->setOption(CURLOPT_SSL_VERIFYPEER, 1);
		} //end function enableSSLVerification()
		
		/**
		 * get the information according to $option
		 *
		 * @param int $option the constant of the searched option
		 *
		 * @return string|null the value of the given option as <b>string</b><br/><b>false</b> on failure
		 */
		public function getInfo( int $option ) :?string {
			$toRet = curl_getinfo($this->init, $option);
			if( $toRet === false ){
				return null;
			}
			return $toRet;
		} //end function getInfo()
		
		/**
		 * get the option-value according to $option
		 *
		 * @param int $option the constant of the searched option
		 *
		 * @return string|null the value of the given option as <b>string</b><br/><b>false</b> on failure
		 */
		public function getOption( int $option ) :?string {
			$toRet = $this->getInfo($option);
			if( $toRet === false ){
				return null;
			}
			return $toRet;
		}
		
		/**
		 * set the http method of the request
		 *
		 * @param string $method post, get, put or delete
		 *
		 * @throws Exception
		 */
		public function setRequestMethod( string $method ){
			switch( strtolower($method) ){
				case 'post':
					curl_setopt($this->init, CURLOPT_CUSTOMREQUEST, 'POST');
					break;
				case 'get':
					curl_setopt($this->init, CURLOPT_CUSTOMREQUEST, 'GET');
					break;
				case 'put':
					curl_setopt($this->init, CURLOPT_CUSTOMREQUEST, 'PUT');
					break;
				case 'delete':
					curl_setopt($this->init, CURLOPT_CUSTOMREQUEST, 'DELETE');
					break;
				default:
					throw new Exception(sprintf('%s::%s expect first parameter to be post, get, put or delete. "%s" was given', __CLASS__, __FUNCTION__, $method));
			}
		} //end function setRequestMethod()
		
		/**
		 * set the address, where to send the next request
		 *
		 * @param string $url <b>string</b> the url to send the request to
		 *
		 * @return bool|null <b>true</b> on success or <b>false</b> on failure. <b>null</b> if the validation of the url failed.
		 */
		public function setTargetAddress( string $url ) :?bool{
			if( filter_var($url, FILTER_VALIDATE_URL) === false ){
				$this->setLastError('000-000-036', sprintf('The given URL at %s::%s is not a valid url', __CLASS__, __FUNCTION__));
				return null;
			}
			return curl_setopt($this->init, CURLOPT_URL, $url);
		} //end function setRequestAddress()
		
		//endregion
		
		//region error handling
		const ERROR_CODE = 0;
		const ERROR_MSG = 1;
		const ERROR_PREVIOUS = 2;
		
		private array $errorList = array();
		
		/**
		 * set the given code and msg as last error that occurred
		 *
		 * @param string $code
		 * @param string $msg
		 */
		protected function setLastError( string $code, string $msg ){
			$this->errorList[] = array(
				self::ERROR_CODE => $code,
				self::ERROR_MSG => $msg,
				self::ERROR_PREVIOUS => null,
			);
		} //end function setLastError()
		
		public function getErrorList() :array{
			return $this->errorList;
		}
		
		/**
		 * get the last error that occurred.
		 *
		 * @return array the last error that occurred
		 * @internal to access the information's use the ERROR_* class-constants
		 */
		public function getLastError() :array{
			$c = count($this->errorList) - 1;
			if( $c < 0 ){
				return array();
			}
			return $this->errorList[ $c ];
		} //end function getLastError()
		//endregion
		
		//region cookie handling
		
		/** @var array|null */
		private ?array $lastCookies = null;
		private const COOKIE_NAME = 0;
		private const COOKIE_VALUE = 1;
		
		/**
		 * get the last cookie header, received from the last response
		 *
		 * @param string|null $name the name of the cookie
		 *
		 * @return array|string|false|null <b>array</b> containing all cookies if $name is not given<br/>
		 * <b>string</b> if $name is given as value of the requested cookie<br/>
		 * <b>false</b> if $name is given but the requested cookie dose not exists<br/>
		 * <b>null</b> on failure. In this case the function "getLastError()" can be used to get more information's
		 */
		public function getCookie( string $name = null ) :array|string|false|null{
			if( $this->lastCookies === null ){
				if( $this->getCookiesFromResponseHeader() === false ){
					$this->setLastError(0, 'Could not receive cookie information from response header');
					return null;
				}
				
			}
			
			if( $name === null ){
				return $this->lastCookies;
				
			} else if( !isset($this->LastCookies[ $name ]) ) {
				return false;
				
			}
			
			return $this->lastCookies[ $name ];
		} //end function getCookie()
		
		/**
		 * parse all cookies from the last ResponseHeader and save it into
		 *
		 * @return array|null <b>array</b> of cookies on success<br/><b>null</b> on failure
		 */
		private function getCookiesFromResponseHeader() :?array{
			if( $this->responseHeader === null ){
				$this->setLastError(0, 'No  response header is set.');
				return null;
			}
			
			$matches = array();
			if( preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $this->responseHeader, $matches) === false ){
				$this->setLastError(0, 'No cookies found.');
				return null;
				
			} else if( empty($matches) ) {
				$this->setLastError(0, 'No cookies was found.');
				return null;
				
			} else if( !isset($matches[ 1 ]) ) {
				$this->setLastError(0, 'Result of cookie header was not as expected');
				return null;
			}
			
			$cookies = array();
			foreach( $matches[ 1 ] as $cookie ){
				$result = explode('=', $cookie);
				if( empty($result) ){
					continue;
					
				} else if( count($result) != 2 ) {
					$this->setLastError(0, 'Could not determinate cookie from cookie-string "' . $cookie . '".');
					continue;
				}
				
				$cookies[ $result[ self::COOKIE_NAME ] ] = $result[ self::COOKIE_VALUE ];
				
			} //end foreach $match[1]
			$this->lastCookies = $cookies;
			return $cookies;
		} //end function getCookiesFromResponseHeader()
		
		//endregion
		
		//region request header
		/** @var array $requestHeader */
		private array $requestHeader = array();
		
		/**
		 * add all options to the existing request header
		 *
		 * @param array $header an array, where the key represents the name of the header-field and the value the header-value
		 */
		public function addRequestHeaderByArray( array $header ){
			$this->requestHeader = array_merge(
				$this->requestHeader,
				array_change_key_case($header, CASE_LOWER)
			);
		} //end function addRequestHeaderByArray()
		
		/**
		 * add ( overwrite if exists ) the option $name with value $value to the next send header
		 *
		 * @param string $name
		 * @param string $value
		 */
		public function addRequestHeader( string $name, string $value ){
			$this->addHeaderInfo($name, $value);
		}
		
		/**
		 * overwrite all options in the current request header and set $header as new one
		 *
		 * @param array $header an array, where the key represents the name of the header-field and the value the header-value
		 */
		public function setRequestHeaderByArray( array $header ){
			$this->requestHeader = array_change_key_case($header, CASE_LOWER);
		}
		
		/**
		 * is an alias for addRequestHeader()
		 *
		 * @param string $name
		 * @param string $value
		 */
		public function setRequestHeader( string $name, string $value ){
			$this->addRequestHeader($name, $value);
		}
		
		/**
		 * resets all information the current request-header
		 */
		public function resetRequestHeader(){
			$this->requestHeader = array();
		}
		
		/**
		 * get the last send request header<br/>
		 * to get the information, that will be used for the next request use <i></i>
		 *
		 * @return mixed
		 */
		public function getRequestHeader() :string{
			return curl_getinfo($this->init, CURLINFO_HEADER_OUT);
		}
		
		/**
		 * get the information that will be used as header information, in the next request
		 *
		 * @return array
		 */
		public function getCurrentRequestHeader() :array{
			return $this->requestHeader;
		}
		
		/**
		 * prepares / format the current header information and apply them for the next request
		 */
		private function setHeader(){
			$tmp = array();
			//$this->requestHeader[ 'content-length' ] = $this->contentLength;
			foreach( $this->requestHeader as $name => $value ){
				$tmp[] = sprintf('%s: %s', $name, $value);
			}
			curl_setopt($this->init, CURLOPT_HTTPHEADER, $tmp);
		}
		
		/**
		 * @param string $headerField
		 * @param string $value
		 */
		private function addHeaderInfo( string $headerField, string $value ){
			$this->requestHeader[ strtolower($headerField) ] = $value;
		} //end function setHeaderInfo()
		//endregion
		
		//region response header
		/** @var string|null the header of the last executed request */
		private ?string $responseHeader = null;
		
		/** @var array|null the parsed header of the last executed request */
		private ?array $responseHeaderArray = null;
		
		/**
		 * get the http-response-code for the last send request
		 *
		 * @return int|null
		 */
		public function getResponseCode() :?int{
			$code = $this->getInfo(CURLINFO_RESPONSE_CODE);
			if( $code === false ){
				return null;
			}
			return intval($code);
		}
		
		/**
		 * returns the header of the last request
		 *
		 * @return string|null <b>string<b/> as header of the last request.<br/><b>null</b> if no request has been made or the last one failed.
		 */
		public function getResponseHeader() :?string{
			return $this->responseHeader;
		}
		
		/**
		 * parse the response header and return the information as array. Excluding "Set-Cookie"
		 *
		 * @return array|null an array where the key represents the field-name ( lower case ) and the value the field-value
		 */
		public function getResponseHeaderAsArray() :?array{
			if( $this->responseHeaderArray !== null ){
				return $this->responseHeaderArray;
			}
			$header = $this->getResponseHeader();
			if( $header === null ){
				return null;
			}
			$headerLines = explode(PHP_EOL, $header);
			$toRet = array();
			foreach( $headerLines as $headerLine ){
				$pos = strpos($headerLine, ':');
				if( $pos === false ){
					continue;
				}
				$name = trim(strtolower(substr($headerLine, 0, $pos)));
				if( $name == 'set-cookie' ){
					continue;
				}
				$value = substr($headerLine, $pos + 1);
				$toRet[ $name ] = trim($value);
			}
			$this->responseHeaderArray = $toRet;
			return $toRet;
		}
		
		//endregion
		
		//region request body
		
		//out-of-the-box supported context-types
		public const CONTENT_TYPE_JSON = 'application/json';
		public const CONTENT_TYPE_PDF = 'application/pdf';
		public const CONTENT_TYPE_PLAIN = 'text/plain';
		public const CONTENT_TYPE_XML = 'text/xml';
		public const CONTENT_TYPE_URL = 'application/x-www-form-urlencoded';
		
		/**
		 * set the content-type information to $value
		 * is a shortcut for setHeaderInfo('Content-Type', $value);
		 *
		 * @param string $value
		 */
		public function setContentType( string $value ){
			$this->addHeaderInfo('Content-Type', $value);
		} //end function setContentType()
		
		/**
		 * set the content-type information to application/json
		 */
		public function setContentTypeJson(){
			$this->setContentType(self::CONTENT_TYPE_JSON);
		}
		
		/**
		 * set the content-type information to text/plain
		 */
		public function setContentTypePlain(){
			$this->setContentType(self::CONTENT_TYPE_PLAIN);
		}
		
		/**
		 * set the content-type information to application/pdf
		 */
		public function setContentTypePDF(){
			$this->setContentType(self::CONTENT_TYPE_PDF);
		}
		
		/**
		 * set the content-type information to text/xml
		 */
		public function setContentTypeXml(){
			$this->setContentType(self::CONTENT_TYPE_XML);
		}
		
		/**
		 * set the content-type information to application/x-www-form-urlencoded ( default, if non given )
		 */
		public function setContentTypeUrl(){
			$this->setContentType(self::CONTENT_TYPE_URL);
		}
		
		
		/**
		 * is either <ul>
		 * <li><b>string</b> used for XML, plain, pdf, json, etc. </li>
		 * <li><b>array</b> used for form-url-encoded, multipart, etc. </li>
		 * </ul>
		 * _setRequestBody() is expected to return <b>null</b> if the content-type does not match the requestBody<br/>
		 * keep in mind, that only the out-of-the-box supported context-types can be checked for
		 *
		 * @var array|string
		 */
		private array|string $requestBody = array();
		
		/**
		 * prepare / format the request body according to the content-type
		 *
		 * @return bool
		 */
		private function _setRequestBody() :bool{
			if( !array_key_exists('content-type', $this->requestHeader) ){
				$this->requestHeader[ 'content-type' ] = self::CONTENT_TYPE_URL;
			}
			switch( $this->requestHeader[ 'content-type' ] ){
				case self::CONTENT_TYPE_JSON:
					$body = json_encode($this->requestBody);
					break;
				
				case self::CONTENT_TYPE_URL:
					if( is_array($this->requestBody) ){
						$body = $this->buildPostStringFromArray($this->requestBody);
						break;
					}
					$body = $this->requestBody;
					break;
				
				case self::CONTENT_TYPE_XML:
				case self::CONTENT_TYPE_PDF:
					/** @noinspection PhpMissingBreakStatementInspection */
				case self::CONTENT_TYPE_PLAIN:
					if( !is_string($this->requestBody) ){
						$this->setLastError('000-000-04E', sprintf('Invalid request-body-type "%s" found for %s', gettype($this->requestBody), $this->requestHeader[ 'content-type' ]));
						return false;
					}
				
				default:
					$body = $this->requestBody;
			} //end switch
			
			return curl_setopt($this->init, CURLOPT_POSTFIELDS, $body);
		}
		
		/**
		 * add ( overwrite if already exists ) the post-value $name with $value<br/>
		 * if the current requestBody-value is a string, the current value will be removed and replaced with an array<br/>
		 *
		 * @param string $name the name of the value
		 * @param string $value the value
		 */
		public function addPostValue( string $name, string $value ){
			if( !is_array($this->requestBody) ){
				$this->requestBody = array();
			}
			$this->requestBody[ $name ] = $value;
		}
		
		/**
		 * add ( overwrite if value already exists ) the post data with $postData<br/>
		 * if the current requestBody-value is a string, the current value will be removed and replaced with an array
		 *
		 * @param array $postData an array of strings where the key represents the post name and the value the post value
		 */
		public function addPostValues( array $postData ){
			if( !is_array($this->requestBody) ){
				$this->requestBody = $postData;
				return;
			}
			$this->requestBody = array_merge($this->requestBody, $postData);
		}
		
		/**
		 * overwrite the values of the request body with $postValues
		 *
		 * @param array $postValues an array of strings where the key represents the post name and the value the post value
		 */
		public function setPostValues( array $postValues ){
			$this->setRequestBody($postValues);
		} //end function setPostValues()
		
		/**
		 * overwrite the values of the request body with $body<br/>
		 * If content type is supposed to be<ul>
		 * <li><b>json-array</b>, give the data as an array</li>
		 * <li><b>json-string</b>, give the plain string ( not encoded )</li>
		 * <li><b>form-urlencoded</b>, give the data as array</li>
		 * <li><b>PDF</b>, give the data as string</li>
		 * <li><b>etc</b>, give the data as string</li>
		 * </ul>
		 *
		 * @param string|array $body <b>array</b> of strings where the key represents the post name and the value the post value<br/>
		 * <b>string</b> the new request body value.
		 */
		public function setRequestBody( string|array $body ){
			$this->requestBody = $body;
		}
		
		/**
		 * build a post string for a post request from a given array
		 *
		 * @param array $array an array of strings where the key represents the post name and the value the post value
		 *
		 * @return string <b>string</b> on success<br/><b>false</b> if sub string() encounters any problems
		 */
		public function buildPostStringFromArray( array $array ) :string{
			$postString = '';
			foreach( $array as $postName => $postValue ){
				$postString .= '&' . $postName . '=' . $postValue;
			}
			if( strlen($postString) > 0 ){
				$postString = substr($postString, 1); //removing the first "&"
			}
			
			return $postString;
		} //end function buildPostStringFromArray()
		//endregion
		
		//region response body
		/** @var string|null the body of the last executed request */
		private ?string $responseBody = null;
		
		/**
		 * returns the body of the last request
		 *
		 * @return string|null <b>string</b> as body content of the last request<br/><b>null</b> if no request has been made or the last one failed
		 */
		public function getResponseBody() :?string{
			return $this->responseBody;
		} //end function getResponseBody()
		//endregion
		
		/**
		 * sends the configured request
		 *
		 * @return string|null the body of the response as <b>string</b><br/>
		 * <b>false</b> on failure. In this case the function "getLastError()" can be used to gain more information's
		 */
		public function execute() :?string{
			$this->setHeader();
			if( !$this->_setRequestBody() ){
				$this->setLastError('000-000-04F', 'Failed to set request body');
			}
			
			$response = curl_exec($this->init);
			if( $response === false ){
				$this->responseHeader = null; //remove old information's
				$this->responseBody = null; //remove old information's
				$this->setLastError(curl_errno($this->init), curl_error($this->init));
				return null;
			}
			
			$headerSize = curl_getinfo($this->init, CURLINFO_HEADER_SIZE);
			$this->responseHeader = substr($response, 0, $headerSize);
			$this->responseHeaderArray = null;
			$this->lastCookies = null; //reset cookie information's. Will be parsed if needed
			
			$this->responseBody = substr($response, $headerSize);
			return $this->responseBody;
		}
		
	} //end class
} //end namespace
